const mongoose = require('mongoose');
const chalk = require('chalk')
const dbUrl = "mongodb://localhost:27017/document";
// const dbUrl = mongoose.connect('mongodb://localhost:27017/document', {useNewUrlParser: true});

const connectMongo = () => {
  const connected = chalk.bold.cyan,
  error = chalk.bold.yellow,
  disconnected = chalk.bold.red,
  termination = chalk.bold.magenta;

  mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  
  mongoose.connection.on('connected', () => {
    console.log(connected(`Mongoose connection is open to ${dbUrl}`))
  })
  mongoose.connection.on('error', (err) => {
    console.log(error(`Mongoose connection has occured ${err} error`))
  })
  mongoose.connection.on('disconnected', () => {
    console.log(disconnected(`Mongoose connection is disconnected`))
  })
  process.on('SIGNIT', () => {
    mongoose.connection.close( () => {
      console.log(termination(`Mongoose connection is disconnected due to application termination`))
      process.exit(0)
    })
  })
}

module.exports = { connectMongo };
