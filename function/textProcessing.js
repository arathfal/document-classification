const Words = require('./model').wordModel;

const findWord = async (params) => { 
    try {
        return await Words.findOne(params)
    } catch(err) {
        console.log(err)
    }
}
const cek = async (text) => {
    const data = await findWord({word: text})
    return data;
}
const inflectionalSuffix = async (text) => {
    let result = text;
    let cekDb = await cek(text);
    if(cekDb) {
        result = text;
    }else if(text.slice(-3) == 'lah' || text.slice(-3) == 'kah' || text.slice(-3) == 'kah' || text.slice(-3) == 'tah' || text.slice(-3) == 'pun' || text.slice(-3) == 'nya')  {
        result = text.slice(0, -3);
        cekDb = await cek(result);
        if(cekDb) {
            result = result;
        }
    }else if(text.slice(-2) == 'ku' || text.slice(-2) == 'mu') {
        result = text.slice(0, -2);
        cekDb = await cek(result);
        if(cekDb) {
            result = result;
        }
    }
    return result;
}
const derivationSuffix = async (text) => {
    let result = text;
    let cekDb = await cek(text);
    if(cekDb) {
        result = text;
    }else {
        if(text.slice(-1) == 'i') {
            result = text.slice(0, -1);
            cekDb = await cek(result);
            if(cekDb) {
                result = result;
            }else {
                result = text;
            }
        }else if(text.slice(-2) == 'an') {
            result = text.slice(0, -2);
            cekDb = await cek(result);
            if(cekDb) {
                result = result;
            }else {
                result = text
            }
            if(result.slice(-3) == 'kan') {
                result = result.slice(0, -3);
                cekDb = await cek(result);
                if(cekDb) {
                    result = result;
                }else {
                    result = text;
                }
            }
        }else {
            result = text;
        }
    }
    return result;
}
const derivationPrefix = async (text) => {
    let result = text;
    let cekDb = await cek(text);
    if(cekDb) {
        result = text;
    }else {
        if(text.slice(0, 2) == 'di' || text.slice(0, 2) == 'ke' || text.slice(0, 2) == 'se') {
            result = text.slice(2);
            cekDb = await cek(result);
            if(cekDb) {
                result = result;
            }else {
                result = await derivationSuffix(result);
                cekDb = await cek(result);
                if(cekDb) {
                    result = result;
                }else {
                    result = text;
                    if(result.slice(0, 5) == 'diper' || result.slice(0, 5) == 'diber' || result.slice(0, 5) == 'keber' || result.slice(0, 5) == 'keter') {
                        result = result.slice(5);
                        cekDb = await cek(result);
                        if(cekDb) {
                            result = result;
                        }else {
                            result = await derivationSuffix(result);
                            cekDb = await cek(result);
                            if(cekDb) {
                                result = result;
                            }else {
                                result = text; 
                            }
                        }
                    }
                } 
            }
        }else if(text.slice(0, 2) == 'be' || text.slice(0, 2) == 'te') {
            result = text.slice(2);
            cekDb = await cek(result);
            if(cekDb) {
                result = result;
            }else {
                result = await derivationSuffix(result);
                cekDb = await cek(result);
                if(cekDb) {
                    result = result;
                }else {
                    result = text;
                    if(result.slice(0, 3) == 'bel' || result.slice(0, 3) == 'ber' || result.slice(0, 3) == 'tel' || result.slice(0, 3) == 'ter') {
                        result = result.slice(3);
                        cekDb = await cek(result);
                        if(cekDb) {
                            result = result;
                        }else {
                            result = await derivationSuffix(result);
                            cekDb = await cek(result);
                            if(cekDb) {
                                result = result;
                            }else {
                                result = text; 
                            }
                        }
                    }
                } 
            }
        }else if(text.slice(0, 2) == 'me' || text.slice(0, 2) == 'pe') {
            result = text.slice(2);
            cekDb = await cek(result);
            if(cekDb) {
                result = result;
            }else {
                result = await derivationSuffix(result);
                cekDb = await cek(result);
                if(cekDb) {
                    result = result;
                }else {
                    result = text;
                    if(result.slice(0, 6) == 'mempel') {
                        result = result.slice(6);
                        cekDb = await cek(result);
                        if(cekDb) {
                            result = result;
                        }else {
                            result = await derivationSuffix(result);
                            cekDb = await cek(result);
                            if(cekDb) {
                                result = result;
                            }else {
                                result = text; 
                            }
                        }
                    }
                    if(result.slice(0, 6) == 'memper') {
                        result = result.slice(6);
                        cekDb = await cek(result);
                        if(cekDb) {
                            result = result;
                        }else {
                            result = await derivationSuffix(result);
                            cekDb = await cek(result);
                            if(cekDb) {
                                result = result;
                            }else {
                                result = text; 
                            }
                        }
                    }
                    if(result.slice(0, 6) == 'pembel') {
                        result = result.slice(6);
                        cekDb = await cek(result);
                        if(cekDb) {
                            result = result;
                        }else {
                            result = await derivationSuffix(result);
                            cekDb = await cek(result);
                            if(cekDb) {
                                result = result;
                            }else {
                                result = text; 
                            }
                        }
                    }
                    if(result.slice(0, 4) == 'meng' || result.slice(0, 4) == 'peng') {
                        result = result.replace(result.slice(0, 4), "k")
                        cekDb = await cek(result);
                        if(cekDb) {
                            result = result;
                        }else {
                            result = await derivationSuffix(result);
                            cekDb = await cek(result);
                            if(cekDb) {
                                result = result;
                            }else {
                                result = text;
                                result = result.slice(4);
                                cekDb = await cek(result);
                                if(cekDb) {
                                    result = result;
                                }else {
                                    result = await derivationSuffix(result);
                                    cekDb = await cek(result);
                                    if(cekDb) {
                                        result = result;
                                    }else {
                                        result = text; 
                                    }
                                }
                            }
                        }
                    }
                    if(result.slice(0, 4) == 'meny' || result.slice(0, 4) == 'peny') {
                        result = result.replace(result.slice(0, 4), "s")
                        cekDb = await cek(result);
                        if(cekDb) {
                            result = result;
                        }else {
                            result = await derivationSuffix(result);
                            cekDb = await cek(result);
                            if(cekDb) {
                                result = result;
                            }else {
                                result = text;
                                result = result.slice(4);
                                cekDb = await cek(result);
                                if(cekDb) {
                                    result = result;
                                }else {
                                    result = await derivationSuffix(result);
                                    cekDb = await cek(result);
                                    if(cekDb) {
                                        result = result;
                                    }else {
                                        result = text; 
                                    }
                                }
                            }
                        }
                    }
                    if(result.slice(0, 3) == 'mel' || result.slice(0, 3) == 'mer' || result.slice(0, 3) == 'pel' || result.slice(0, 3) == 'per') {
                        result = result.slice(3);
                        cekDb = await cek(result);
                        if(cekDb) {
                            result = result;
                        }else {
                            result = await derivationSuffix(result);
                            cekDb = await cek(result);
                            if(cekDb) {
                                result = result;
                            }else {
                                result = text; 
                            }
                        }
                    }
                    if(result.slice(0, 3) == 'men' || result.slice(0, 3) == 'pen') {
                        result = result.replace(result.slice(0, 3), "t")
                        cekDb = await cek(result);
                        if(cekDb) {
                            result = result;
                        }else {
                            result = await derivationSuffix(result);
                            cekDb = await cek(result);
                            if(cekDb) {
                                result = result;
                            }else {
                                result = text;
                                result = result.slice(3);
                                cekDb = await cek(result);
                                if(cekDb) {
                                    result = result;
                                }else {
                                    result = await derivationSuffix(result);
                                    cekDb = await cek(result);
                                    if(cekDb) {
                                        result = result;
                                    }else {
                                        result = text; 
                                    }
                                }
                            }
                        }
                    }
                    if(result.slice(0, 3) == 'mem' || result.slice(0, 3) == 'pem') {
                        result = result.replace(result.slice(0, 3), "p")
                        cekDb = await cek(result);
                        if(cekDb) {
                            result = result;
                        }else {
                            result = await derivationSuffix(result);
                            cekDb = await cek(result);
                            if(cekDb) {
                                result = result;
                            }else {
                                result = text;
                                result = result.slice(3);
                                cekDb = await cek(result);
                                if(cekDb) {
                                    result = result;
                                }else {
                                    result = await derivationSuffix(result);
                                    cekDb = await cek(result);
                                    if(cekDb) {
                                        result = result;
                                    }else {
                                        result = text; 
                                    }
                                }
                            }
                        }
                    }
                }   
            }
        }
    }
    return result;
}

const stemming = async (text) => {
    let result = [];
    for (let key of text) {
        const prefix = await inflectionalSuffix(key);
        const prefixTwo = await derivationSuffix(prefix);
        const final = await derivationPrefix(prefixTwo);
        result.push(final);
    }
    return result;
}
const a = ['ajar',
'belajar',
'beli',
'berlari',
"dilakukan",
"dimaksud",
"digunakan",
"dimiliki",
"dikuasai",
"diminta",
"diperlukan",
"dikenai",
"dipergunakan",
"diberikan",
"didik",
"dididik",
"pendidikan",
"indonesia",
"negeri",
"sesuai",
"pelaksana",
"lakukan",
"pegawai",
"belajar",
"mempelajari",
"mengajar",
'menyapu',
'mengontrol',
'memesona',
"pembelajaran"]
// const x = async () => {
//   const y = await stemming(a);
//   console.log(y);
// };
// x();

const textProcessing = async (text) => {
    try {
      const stopword = await fs
        .readFileSync("function/stopword.txt", "utf-8")
        .replace(/[\r]/g, "");
      const fixStopword = await stopword.replace(/[\n]/g, " ");
      const listStopword = await fixStopword.split(" ");
      /* Remove Line Break, Punctuation, Stopword */
      const removeLB = await text.trim().replace(/[\n-.]/g, " ");
      const removeTB = await removeLB.replace(/[^a-zA-z]/g, " ");
      let removeChar = await removeTB.replace(/\s[a-zA-Z]\s/g, " ");
      removeChar = removeChar.replace(
        new RegExp("\\b(" + listStopword.join("|") + ")\\b", "g"),
        ""
      );
      const removeDoubleSpace = await removeChar.replace(/\s{2,}/g, " ");
      /* Tokenizing and Case Folding then Store to Array */
      const token = await removeDoubleSpace.toLowerCase().split(" ");
      var result = await stemming(token);
    } catch (error) {
      throw error;
    }
    return result;
  };
  
  module.exports = { textProcessing };
