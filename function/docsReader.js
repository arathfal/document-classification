const fs = require("fs");
const pdf = require("pdf-parse");
const docsPath = require("./docsPath").docsPath;

const contentText = async () => {
  try {
    var pdfText = [];
    for (const key in docsPath) {
      const dataBuffer = await fs.readFileSync(docsPath[key]["path"]);
      const dataPdf = await pdf(dataBuffer);
      const text = await dataPdf.text;
      pdfText.push({
        path: docsPath[key]["path"],
        name: docsPath[key]["name"],
        text: text,
      });
    }
  } catch (e) {
    throw e;
  }

  return pdfText;
};

const mainText = async () => {
  try {
    var pdfText = [];
    for (const key in docsPath) {
      const dataBuffer = await fs.readFileSync(docsPath[key]["path"]);
      const dataPdf = await pdf(dataBuffer);
      const text = await dataPdf.text;
      pdfText.push({
        path: docsPath[key]["path"],
        name: docsPath[key]["name"],
        text: text,
      });
    }
  } catch (e) {
    throw e;
  }

  return pdfText;
};

module.exports = { contentText, mainText };
