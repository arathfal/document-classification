const extractText = require("./docsReader");
const textPros = require("./textProcessing");
const compute = require("./computeTerm");
const util = require("util");
const nn = require("../function/neuralNetwork")

const main = () => {
  const pdf = extractText.mainText();
  const preprocessing = Object.values(pdf).map(async (key) => {
    const textProcessing = await textPros.textProcessing(key["text"]);
    const all = await compute.computeAllWord(textProcessing)
    const term = await compute.termFrequenty(textProcessing)
    const sort = await compute.sortFrequenty(term, all)
    return {
      path: key["path"],
      name: key["name"],
      term: sort
    }
  })
  
  let dataset = []
  for (const [d, buffer] of preprocessing.entries()) {

  }

  const jst = new nn()
  let prediction = jst.prediction(input, weightsW, biasZ, weightsV, biasY)
  if(prediction >= 0 && prediction <0.1) {
    prediction = "Agama"
  }else if(prediction >= 0.1 && prediction <0.2) {
    prediction = "Kesehatan"
  }else if(prediction >= 0.2 && prediction <0.3) {
    prediction = "Pendidikan dan Kebudayaan"
  }else if(prediction >= 0.3 && prediction <0.4) {
    prediction = "Agraria"
  }else if(prediction >= 0.4 && prediction <=0.5) {
    prediction = "Pertahanan"
  }else {
    prediction = "Tidak ada"
  }
  const result = new Prediction({
    path: path,
    name: name,
    division: prediction
  })
  result.save( (err, suc) => {
    if(err) throw err
  })
}


// computeWord();
module.exports = main;
