class NeuralNetwork {

    // Activation Function
    sigmoid(x, derivative) {
      const e = 2.718281828459045;
      let fx = 1 / (1 + e ** -x);
      return (derivative ? fx * (1 - fx) : fx);
    }
  
    lossFuction(prediction, target, derivative) {
      return (derivative ? ((2*prediction) - (2*target)) : Math.sqrt((prediction - target) ** 2))
    }
  
    // Forward Pass
    toHiddenLayer(data, weights, bias) {
      let multiply = 0,
      sum = 0,
      hidden = []
  
      for (const [w, weight] of weights.entries()) {
        for (const [x, input] of data.entries()) {
          multiply = weight[x] * input
          sum += multiply
        }
        hidden.push(this.sigmoid(sum + bias[w], false))
        sum = 0
      }
      return hidden;
    }
  
    toOutputLayer(data, weights, bias) {
      let sum = 0,
      prediction = 0
  
      for (const [v, weight] of weights.entries()) {
        for (const [z, hidden] of data.entries()) {
          sum += weight[z] * hidden
          prediction = this.sigmoid(sum + bias[v], false) 
        }
      }
  
      return prediction;
    }
  
    // Backpropagation
    backpropagation(data, prediction, target, weightsW, biasZ, hidden, weightsV, biasY, alpha) {
      let turunan_loss_y = this.lossFuction(prediction, target, true),
      // let turunan_loss_y = prediction - target,
      turunan_y_y = this.sigmoid(prediction, true),
      turunan_y_v = [],
      turunan_y_bj = biasY,
      gradient_v = [],
      gradient_bj = turunan_loss_y * turunan_y_y * turunan_y_bj,
      turunan_loss_z = [],
      turunan_z_z = [],
      turunan_z_w = [],
      z_w = [],
      turunan_z_bj = [],
      gradient_w = [],
      loss_w = [],
      gradient_bi = []
  
      for (const [z, hide] of hidden.entries()) {
        turunan_y_v.push(hide);
        turunan_z_z.push(this.sigmoid(hide, true))
        for (const [x, input] of data.entries()) {
          z_w.push(input)
        }
        turunan_z_w.push(z_w)
        z_w = []
        turunan_z_bj.push(biasZ[z])
      }
      
      for (const [tV, tYV] of turunan_y_v.entries()) {
        gradient_v.push(turunan_loss_y * turunan_y_y * tYV)
      }
  
      // console.log(`turunan_loss_y : ${turunan_loss_y}`)
      // console.log(`turunan_y_y : ${turunan_y_y}`)
      // console.log(`turunan_y_v : ${turunan_y_v}`)
      // console.log(`turunan_y_bj : ${turunan_y_bj}`)
      // console.log(`gradient_v : ${gradient_v}`)
      // console.log(`gradient_bj : ${gradient_bj}`)
  
      for (const [v, weight] of weightsV.entries()) {
        for (const[gV, gradient] of gradient_v.entries()) {
          turunan_loss_z.push(gradient * weight[gV]);
          weight[gV] -= alpha * gradient
        }
        biasY[v] -= alpha * gradient_bj;
      }
      for (const [zw, turunanZW] of turunan_z_w.entries()) {
        for (const [tzw, tZW] of turunanZW.entries()) {
          loss_w.push(turunan_loss_z[zw] * turunan_z_z[zw] * tZW)
        }
        gradient_bi.push(turunan_loss_z[zw] * turunan_z_z[zw] * turunan_z_bj[zw])
        gradient_w.push(loss_w)
        loss_w = []
      }
  
      // console.log(`turunan loss / z`)
      // console.log(turunan_loss_z)
      // console.log(`turunan z / z`)
      // console.log(turunan_z_z)
      // console.log(`turunan z / w`)
      // console.log(turunan_z_w)
      // console.log(`turunan z / bj`)
      // console.log(turunan_z_bj)
      // console.log(`gradient_w`)
      // console.log(gradient_w)
      // console.log(`gradient_bi`)
      // console.log(gradient_bi)
  
      for (const [wv, weights] of weightsW.entries()) {
        for (const [gW, gradient] of gradient_w.entries()) {
          // for (const [w, weight] of weights.entries()) {
            for (const [g, grad] of gradient.entries()) {
              weights[g] -= alpha * grad
            }
          // }
        }
        biasZ[wv] -= alpha * gradient_bi[wv]
      }
    }
  
    training(data, weightsW, biasZ, weightsV, biasY, label, alpha) {
      for (const i of Array(10).keys()) {
        let arrPrediction = []
        console.log
        (`========================================`)
          console.log(`Epoch ke-${i+1}`)
        for (const [x, input] of data.entries()) {
          console.log(`================`)
          console.log(`Input ${input}`)
          const target = label[x]
          // console.log(`Bobot Input : `)
          // console.log(weightsW)
          // console.log(`Bias Hidden : `)
          // console.log(biasZ)
          // console.log(`Bobot Hidden : `)
          // console.log(weightsV)
          // console.log(`Bias Output : `)
          // console.log(biasY)
          const hidden = this.toHiddenLayer(input, weightsW, biasZ)
          const prediction = this.toOutputLayer(hidden, weightsV, biasY)
          console.log(`Target : ${target}`)
          console.log(`================`)
          console.log(`Prediction : ${prediction}`)
          const error = this.lossFuction(prediction, target, false)
          console.log(`Error : ${error}`)
          arrPrediction.push(prediction)
          this.backpropagation(input, arrPrediction, target, weightsW, biasZ, hidden, weightsV, biasY, alpha)
        }
        // const epochError = error / data.length
        // console.log(`================`)
        // console.log(`Error :`)
        // console.log(epochError)
      }
    }
  
    prediction(data, weightsW, biasZ, weightsV, biasY) {
      for (const [x, input] of data.entries()) {
        console.log(`========================================`)
        console.log(`Input ${input}`)
        console.log(`================`)
        const hidden = this.toHiddenLayer(input, weightsW, biasZ)
        const prediction = this.toOutputLayer(hidden, weightsV, biasY)
        console.log(`Prediction : ${prediction}`)
      }
    }
  }

  module.exports=NeuralNetwork