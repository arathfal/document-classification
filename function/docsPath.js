const dirTree = require("directory-tree");
const PATH = require("path");

let dataDocs = {};
/* convert directory to array of object*/
const tree = dirTree(
  "public/document/",
  { extensions: /\.pdf$/ },
  null,
  (item, PATH, stats) => {
    dataDocs = item;
  }
);
const docsPath = dataDocs["children"];
// console.log(docsPath);
module.exports = { dataDocs, docsPath };
