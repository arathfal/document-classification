const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const filesSchema = new Schema({
    path : {
        type: String,
        unique: true
    },
    name : {
        type: String,
        unique: true
    },
    division : String
},{
    timestamps: true
})
const wordSchema = new Schema({
    word: String
})
const oneSchema = new Schema({
    path : {
        type: String, 
        unique: true
    },
    name : {
        type: String, 
        unique: true
    },
    term : [{}],
    target: Number
},{
    timestamps: true
})
const twoSchema = new Schema({
    path : {
        type: String, 
        unique: true
    },
    name : {
        type: String, 
        unique: true
    },
    term : [{}],
    target: Number

},{
    timestamps: true
})
const threeSchema = new Schema({
    path : {
        type: String, 
        unique: true
    },
    name : {
        type: String, 
        unique: true
    },
    term : [{}],
    target: Number

},{
    timestamps: true
})
const fourSchema = new Schema({
    path : {
        type: String, 
        unique: true
    },
    name : {
        type: String, 
        unique: true
    },
    term : [{}],
    target: Number

},{
    timestamps: true
})
const fiveSchema = new Schema({
    path : {
        type: String, 
        unique: true
    },
    name : {
        type: String, 
        unique: true
    },
    term : [{}],
    target: Number
},{
    timestamps: true
})
const filesModel = mongoose.model('Files', filesSchema),
wordModel = mongoose.model('Word', wordSchema),
oneModel = mongoose.model('One', oneSchema),
twoModel = mongoose.model('Two', twoSchema),
threeModel = mongoose.model('Three', threeSchema),
fourModel = mongoose.model('Four', fourSchema),
fiveModel = mongoose.model('Five', fiveSchema);

module.exports = {filesModel, oneModel, twoModel, threeModel, fourModel, fiveModel, wordModel};