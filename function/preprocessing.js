const extractText = require("./docsReader");
const textPros = require("./textProcessings");
const compute = require("./computeTerm");
const util = require("util");
const data = require('./model').oneModel;

const preprocessing = async () => {
  const objText = await extractText.contentText();
  console.log(objText);
  const textProcessing = Object.values(objText).map(async (key) => {
    const resultPros = await textPros.textProcessing(key["text"]);
    const all = await compute.computeAllWord(resultPros);
    const term = await compute.termFrequenty(resultPros);
    const sort = await compute.sortFrequenty(term, all);
    return {
      path: key["path"],
      name: key["name"],
      // sum: all,
      term: sort,
    }
  });
  return Promise.all(textProcessing);
};

const a = async () => {
  const result = await preprocessing();
  console.clear();
  // console.log(util.inspect(result, false, null, true));
  console.log(`${result.length} Document`);
  console.log("Preprocessing have done");
  result.map(value => {
    let output
    if(value['name'].slice(0, 2) == "A_") {
      output = 0.1;
    }else if(value['name'].slice(0, 2) == "B_") {
      output = 0.2;
    }else if(value['name'].slice(0, 2) == "C_") {
      output = 0.3;
    }else if(value['name'].slice(0, 2) == "D_") {
      output = 0.4;
    }else if(value['name'].slice(0, 2) == "E_") {
      output = 0.5;
    }else if(value['name'].slice(0, 2) == "F_") {
      output = 0.6;
    }else if(value['name'].slice(0, 2) == "G_") {
      output = 0.7;
    }else if(value['name'].slice(0, 2) == "H_") {
      output = 0.8;
    }else if(value['name'].slice(0, 2) == "I_") {
      output = 0.9;
    }else if(value['name'].slice(0, 2) == "J_") {
      output = 1.0;
    }else {
      output = 0;
    }
    const dataPdf = new data({
      path: value["path"],
      name: value["name"],
      term : value["term"],
      target: output
    });
    dataPdf.save( (err, doc) => {
      if(err) {
        console.log("Can't save document");
      } else {
        console.log("Successfully save to mongoose");
      }
    });
  })
};
// a();

// preprocessing();

module.exports = { preprocessing };
