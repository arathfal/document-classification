const termFrequenty = async (text) => {
  let term = {};

  text.forEach((key) => {
    term.hasOwnProperty(key) ? term[key]++ : (term[key] = 1);
  });
  return term;
};

const computeAllWord = async (text) => {
  let sum = 0;
  for (let val in text) {
    text[val];
    sum++;
  }
  return sum;
};

const sortFrequenty = async (term, all) => {
  let finalCount = [];
  finalCount = Object.keys(term).map((key) => {
    return {
      word: key,
      total: term[key],
      frequency: parseFloat(term[key] / all).toFixed(3),
    };
  });
  finalCount.sort((a, b) => {
    return b.total - a.total;
  });
  return finalCount;
};
module.exports = { termFrequenty, computeAllWord, sortFrequenty };
