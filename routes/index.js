const express = require("express");
const router = express.Router();
const fs = require('fs')
const path = require('path')
const docs = require("../function/docsPath");
const preprocessing = require("../function/preprocessing")
const mongo = require('../function/dbConfig')
const main = require('../function/main')
const files = require('../function/model').filesModel;
const multer = require('multer')

const Storage = multer.diskStorage({
    destination: "./public/testing/",
    filename: (req, file, cb) => {
      cb(null, `${file.fieldname}_${Date.now()}_${file.originalname}`)
    }
})
const upload = multer({storage: Storage}).single('pdf')


mongo.connectMongo()

/* Route Homepage. */
const pdf = files.find({})
// console.log(pdf);
router.post("/upload", upload, (req, res, next) => {
  const pdfCreate = new files({
    path: `testing/${req.file.filename}`,
    name: req.file.filename
  })
  pdfCreate.save( (err, doc) => {
    if(err) console.log(err);
    res.redirect('back')
  })
})
router.get("/", (req, res, next) => {
  pdf.sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('main', {
      layout: "layouts/index",
      titlePage: "Unclassified Document",
      records: docs,
    })
  });
});
router.post('/classify', async (req, res, next) => {
  res.render(main())
  res.redirect('back')
});
/* End Homepage */


router.get("/agama", async (req, res, next) => {
  files.find({division: "Agama"}).sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('agama', {
      layout: "layouts/index",
      titlePage: "Agama",
      records: docs
    })
  });
});
router.get("/agraria", async (req, res, next) => {
  files.find({division: "Agraria"}).sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('agraria', {
      layout: "layouts/index",
      titlePage: "Agraria",
      records: docs
    })
  });
});
router.get("/kelautan", async (req, res, next) => {
  files.find({division: "Kelautan"}).sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('kelautan', {
      layout: "layouts/index",
      titlePage: "Kelautan",
      records: docs
    })
  });
});
router.get("/kesehatan", async (req, res, next) => {
  files.find({division: "Kesehatan"}).sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('kesehatan', {
      layout: "layouts/index",
      titlePage: "Kesehatan",
      records: docs
    })
  });
});
router.get("/keuangan", async (req, res, next) => {
  files.find({division: "Keuangan"}).sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('keuangan', {
      layout: "layouts/index",
      titlePage: "Keuangan",
      records: docs
    })
  });
});
router.get("/kominfo", async (req, res, next) => {
  files.find({division: "Komunikasi dan Informatika"}).sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('kominfo', {
      layout: "layouts/index",
      titlePage: "Komunikasi dan Informatika",
      records: docs
    })
  });
});
router.get("/lingkungan-hidup", async (req, res, next) => {
  files.find({division: "Lingkungan Hidup"}).sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('lingkungan-hidup', {
      layout: "layouts/index",
      titlePage: "Lingkungan Hidup",
      records: docs
    })
  });
});
router.get("/dikbud", async (req, res, next) => {
  files.find({division: "Pendidikan dan Kebudayaan"}).sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('dikbud', {
      layout: "layouts/index",
      titlePage: "Pendidikan dan Kebudayaan",
      records: docs
    })
  });
});
router.get("/pertahanan", async (req, res, next) => {
  files.find({division: "Pertahanan"}).sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('pertahanan', {
      layout: "layouts/index",
      titlePage: "Pertahanan",
      records: docs
    })
  });
});
router.get("/ristekdikti", async (req, res, next) => {
  files.find({division: "Riset, Teknologi dan Pendidikan Tinggi"}).sort({ createdAt: -1 }).exec(function(err, docs) {
    if(err) throw err
    res.render('ristekdikti', {
      layout: "layouts/index",
      titlePage: "Riset, Teknologi dan Pendidikan Tinggi",
      records: docs
    })
  });
});
module.exports = router;
