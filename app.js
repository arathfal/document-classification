const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const handlebars = require("express-handlebars");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");

const app = express();
app.use(express.static(path.join(__dirname, "public/")));

// view engine setup
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "views"));
app.engine(
  "hbs",
  handlebars({
    layoutsDir: __dirname + "/views/",
    extname: "hbs",
    //new configuration parameter
    partialsDir: __dirname + "/views/partials/",
    helpers: {
      inc: (value, options) => {
        return parseInt(value) + 1;
      },
      ifThen: (cond1, cond2, options) => {
        return (cond1 >= cond2) ? options.fn(this) : options.inverse(this);
      },
    },
  })
);
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/", indexRouter);
app.use("/users", usersRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
