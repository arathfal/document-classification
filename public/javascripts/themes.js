// $(window).on("load", function () {
//     // $("#ctn-preloader").fadeOut();
//     // $("#preloader").delay(1500).fadeOut("slow");
//     // $("body").delay(350).css({ overflow: "visible" });
// });

$(document).ready(function () {
    const activeClass = () => {
        $('.sidebar a').filter(function(){
            return this.href==location.href
        }).addClass('active').siblings().removeClass('active')
    }
    activeClass()

    const rowsPerPage = 7;
    const rows = $("#unclassified tbody tr");
    const rowsCount = rows.length;
    const pageCount = Math.ceil(rowsCount / rowsPerPage);
    const numbers = $("#pagination-unclassified");

    for (let i = 0; i < pageCount; i++) {
        numbers.append('<a href="#">' + (i + 1) + "</a>");
    }

    $("#pagination-unclassified a:first-child").addClass("active");
    const displayRows = (index) => {
        let start = (index - 1) * rowsPerPage;
        let end = start + rowsPerPage;
        rows.hide();
        rows.slice(start, end).show();
    }

    displayRows(1);

    $("#pagination-unclassified a").click(function (e) {
        let $this = $(this);
        e.preventDefault();
        $("#pagination-unclassified a").removeClass("active");
        $this.addClass("active");
        displayRows($this.text());
    });

    $('#classify').on('click', () => {
        $.ajax({
            type: 'POST',
            url: 'http://localhost:3000/classify'
        });
    });
});